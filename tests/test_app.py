import unittest
from flask import Flask
from flask.testing import FlaskClient

class FlaskAppTestCase(unittest.TestCase):
    def setUp(self):
        self.app = Flask(__name__)
        self.app.config['TESTING'] = True


        self.client = self.app.test_client()


    def tearDown(self):

    def test_home_route(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, 'Hello, OTUS!')

if __name__ == '__main__':
    unittest.main()